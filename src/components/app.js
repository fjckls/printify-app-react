import React from 'react';
import { Component } from 'react';

import AppHolder from '../containers/AppHolder';

export default class App extends Component {
  render() {

    return (
      <AppHolder />
    );

  }
}
