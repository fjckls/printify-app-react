import { combineReducers }  		from 'redux';
import Request              		from "./async/reducerRequest";
import {Images, Main, App}      from "./appReducers";




const rootReducer = combineReducers({
	images 						: Images,
	main 							: Main,
	app 							: App,
  menu        			: Request('GET_IMAGES'),
  uploadImage       : Request('UPLOAD_IMAGE	'),
  updateImage      	: Request('UPDATE_IMAGE'),
  options      			: Request('GET_OPTIONS'),
});

export default rootReducer;
