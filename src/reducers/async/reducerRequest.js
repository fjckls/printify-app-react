export default function(namespace){

  return function(state = {isLoading:false, payload:false, error:false}, action){


    
      switch (action.type) {

        case namespace + 'IS_LOADING':

          return Object.assign({}, state, {
            isLoading: action.isLoading,
            error: action.error
          });
          break;

        case namespace + 'RESPONSE_OK':
            return Object.assign({}, state, {
              isLoading: action.isLoading,
              payload: action.payload,
              error:action.error,
            });
            break;

        case namespace + 'HANDLE_ERROR':
            return Object.assign({}, state, {
              isLoading: action.isLoading,
              payload: action.payload,
              error:action.error,
            });
            break;

        default:
          return state;
      }

  }



}
