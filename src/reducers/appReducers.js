export function Images(state = [], action){

  switch ( action.type ) {

      case 'IMAGES':
          return action.payload;
          break;

      default:
        	return state;
  }

}

export function Main(state = { isDragging:false, hover: false }, action){

  switch ( action.type ) {
  	
      case 'MAIN':
          return action.payload;
          break;
       

      default:
        	return state;
  }

}

export function App(state = { current:0}, action){

  switch ( action.type ) {
    
      case 'APP':
          return action.payload;
          break;
       

      default:
          return state;
  }

}