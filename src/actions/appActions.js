import _ from 'lodash';




function getMousePos( event, canvas ){

	let rect = canvas.getBoundingClientRect();
  return {
      x: event.clientX - rect.left,
      y: event.clientY - rect.top
  }	

}



function hitTest( mouse, thumb ){

    if( mouse.x > thumb.x - ( thumb.width * thumb.scale ) / 2 &&
        mouse.x < thumb.x + ( thumb.width * thumb.scale ) / 2 &&
        mouse.y > thumb.y - ( thumb.height * thumb.scale ) / 2 &&
        mouse.y < thumb.y + ( thumb.height * thumb.scale ) / 2 
      ){
      
      return true;
    }

    return false;
}





function updateMainProperties(main){
  return {
    type: 'MAIN',
    payload: main,
  }
}


function updateImages( options ){
  return {
    type: 'IMAGES',
    payload: options,
  }
}


export function updateThumb( options ){

  return function(dispatch, getState){
    
    const state = getState();
    
    let images  = _.cloneDeep(state.images);

    options.map( (settings) => {
      images[2][settings.property] = settings.value;
    });

    dispatch(updateImages(images));
  }

}





export function handleMouseEvents( event, canvas ){

	return function(dispatch, getState){

		const  state = getState();

		let main 		= _.cloneDeep(state.main),
				images 	= _.cloneDeep(state.images);

		
    switch (event.type){
      
      case 'mousedown':

        let pos = getMousePos(event, canvas);
		    if( hitTest(pos, images[2])){
		      main.isDragging   = true;
		      images[2].offsetX = pos.x - images[2].x;
		      images[2].offsetY = pos.y - images[2].y;  
		    }
        
        break;

      case 'mouseup':
      	main.isDragging = false;	
        break;

      case 'mousemove':

        if( main.isDragging && images[2] ){

		    	images[2].x = getMousePos(event, canvas).x - images[2].offsetX;
		    	images[2].y = getMousePos(event, canvas).y - images[2].offsetY;

		  	}
		  	
        break;

      case 'mouseleave':
      	main.isDragging = false;	
        main.hover = false;  
        break;

      case 'mouseover':
        main.hover = true;  
        break;
    }


		dispatch(updateMainProperties(main));
		dispatch(updateImages(images));
		
	}

}



export function  clearImages( ){
    

    return function(dispatch, getState){
      const  state = getState();

      dispatch(updateImages([]));

    }


}




export function  updateOptions( image, index, _this ){
    

    return function(dispatch, getState){

    	const  state = getState();

      const stageSize         = _this.props.stageSize;
      const printCanvasSize   = _this.props.printCanvasSize;
  
    	let images 	=  state.images ? _.cloneDeep(state.images) : [];
    	
    	// GET SCALE FACTOR FOR BIG CANVAS
	    let canvasScaleFactor = { 
	      width   : image.width / printCanvasSize.width, 
	      height  : image.height / printCanvasSize.height 
	    }

	    // INDEX == 2 - THIS IS OUR DRAGGABLE THUMB RESIZE IT TO FIT SMALL CANVAS
    	let options = {
    		image         : image, 
        x             : index == 2 ? _this.props.thumbOptions.x !== false ? _this.props.thumbOptions.x : _this.refs.canvas.width / 2 					: 0, 
        y             : index == 2 ? _this.props.thumbOptions.y !== false ? _this.props.thumbOptions.y : stageSize.top + stageSize.height / 2 : 0, 
        offsetX       : 0, 
        offsetY       : 0, 
        width         : index == 2 ?  canvasScaleFactor.width * stageSize.width   : image.width, 
        height        : index == 2 ?  canvasScaleFactor.height * stageSize.height : image.height, 
        scaledWidth   : image.width, 
        scaledHeight  : image.height, 
        scale         : index == 2 ? _this.props.thumbOptions.scale : 1
    	}
    	
    	images[index] = options;

    	dispatch(updateImages(images));


    }
   
    
}


function updateAppState(payload){

  return {
    type: 'APP',
    payload: payload,
  }
  
}

export function updateApp( options  ){
    

    return function(dispatch, getState){

      const state = getState();
      let app = _.cloneDeep(state.app);

      app[options.name] = options.value;

      dispatch( updateAppState(app) );

    }


}