import axios from 'axios';

// Get root url where all requests will be made
const ROOT_URL   = $('body').data('baseurl');
// Different requests have their own namespace
let   NAMESPACE  = 'default';

function responseOK(response){
  return {
    type: NAMESPACE + 'RESPONSE_OK',
    payload: response,
    error: false,
    isLoading:false,
  }
}


// Toggle loading status and preloader visibility
function loadingChangedStatus(isLoading) {

  return {
    type: NAMESPACE + 'IS_LOADING',
    isLoading:  isLoading,
    error: false,
  }
}

function toggleLoader(isLoading){
  if(isLoading){
    $('.preloader').css('display', 'block');
  }else{
    $('.preloader').css('display', 'none');
  }
}



function handleError(error){

  return {
    type: NAMESPACE + 'HANDLE_ERROR',
    errorID: error,
    isLoading:false,
  }
}







export function makeRequest( namespace, url, params){

  NAMESPACE = namespace;

  return function(dispatch, geState){

      const state = geState();

      dispatch( loadingChangedStatus(true) );

      if( namespace == 'GET_IMAGES'){
        toggleLoader(true);
      }
      


      axios({
          method  : 'post',
          url     : ROOT_URL + url,
          data    : params
      })
      .then(function (response) {

        toggleLoader(false);

        if( response.data.error ){
          return dispatch(handleError(response.data.error));
        }

        dispatch(responseOK(response));

      })
      .catch(function (error) {


        alert('Something went wrong! Check console and request!')
        if (error.response) {
            // The request was made, but the server responded with a status code
            // that falls out of the range of 2xx
            console.log('Error - data :', error.response.data);
            console.log('Error - status :', error.response.status);
            console.log('Error - headers :', error.response.headers);
        } else {
            // Something happened in setting up the request that triggered an Error
            console.log('Error - 2 :', error.message);
        }

        console.log('Error - 3 :',error.config);


        console.log('Error Object ', error)
        
        toggleLoader(false);
        dispatch(handleError('connection_error'));

      });



  }


}
