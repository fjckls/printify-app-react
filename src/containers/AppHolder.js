import React, { Component }       from 'react';
import { connect }                from 'react-redux';
import { bindActionCreators }     from 'redux';


/**
 * ACTION CREATORS
 */
import { makeRequest }              from '../actions/async/actionRequest';
import { updateApp }                from '../actions/appActions';

/**
 * COMPONENTS
 */
import Uploader               from './Uploader';
import Canvas                 from './Canvas';


class AppHolder extends Component{

  constructor(){
    super();

    this.renderCurrentCanvas  = this.renderCurrentCanvas.bind(this);
    this.renderMenu           = this.renderMenu.bind(this);
    this.changeCanvas         = this.changeCanvas.bind(this);

  }


  componentDidMount(){
    this.props.makeRequest('GET_IMAGES', 'handleImages', {});
  }





  changeCanvas(event, index ){
    event.preventDefault();
    this.props.updateApp({name:'current', value:index});
  }




  renderCurrentCanvas(item, index){
    return(
      <span key={ index }>

        { this.props.app.current == index ?


           <Canvas 
            layers          = { ['/images/shirt.png' , '/images/bg.png', item.full_image ] }
            thumbOptions    = { { scale:0, x:0, y:0 } }
            
            id              = { item.id }

            stageSize       = { { width:222,   height:259,  top:79, left:158  } }
            printCanvasSize = { { width:4500,  height:5250          } }
            canvasSize      = { { width:530,   height:530           } }
          />

         : false }

       </span>
    );
  }



  renderMenu(item, index){
    
    return(
      <li key={ index }>
        <a href='#' onClick={ (event) => this.changeCanvas(event, index) }>
          <img 
            className={ this.props.app.current == index ? 'img-thumbnail active-element'  : 'img-thumbnail' } 
            src={ item.thumb } width='150'  />
        </a>
      </li>

    );
  }



  render(){

    let menu = [];

    // MAKE SURE THE MENU HAS FINISHED LOADING
    if( !this.props.menu.isLoading && !this.props.menu.error && this.props.menu.payload  ){
      menu = this.props.menu.payload.data;
    }



    return (
      <span>
        

            <div className='col-md-4'>

              <ul className='menu-holder'>
                <Uploader />
                { menu.map(this.renderMenu) }
              </ul> { /* .menu-holder */}

            </div> { /* .col-md-4 */}



            <div className='col-md-8 clearfix'>

              { menu.map( this.renderCurrentCanvas ) }
        
            </div> { /* .col-md-8 */}


      </span>
    );
  }

}


function mapStateToProps( {  menu, app } ){
  return { menu, app }
}

function mapDispatchToProps( dispatch ){
  return bindActionCreators({ makeRequest, updateApp }, dispatch);
}


export default connect( mapStateToProps, mapDispatchToProps )( AppHolder );
