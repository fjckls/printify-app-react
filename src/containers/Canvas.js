import React, { Component }       from 'react';
import { connect }                from 'react-redux';
import { bindActionCreators }     from 'redux';


/**
 * ACTION CREATORS
 */
import { makeRequest }                                      from '../actions/async/actionRequest';
import { updateOptions, handleMouseEvents, updateThumb, clearImages }    from '../actions/appActions';

/**
 * COMPONENTS
 */



class Canvas extends Component{

  constructor(){
    super();

    this.mouseEvent   = this.mouseEvent.bind(this);
    this.updateScale  = this.updateScale.bind(this);
    this.draw         = this.draw.bind(this);
    this.loadAssets   = this.loadAssets.bind(this);
    this.saveArtwork  = this.saveArtwork.bind(this);

    this.ctx              = false;
    this.optionsLoading   = false;
    
  }





  componentDidMount(){

    this.ctx = this.refs.canvas.getContext('2d');
    this.ctx.imageSmoothingEnabled = true;
        
    // fixes a problem where double clicking causes text to get selected on the canvas
    this.refs.canvas.addEventListener('selectstart',  function(e) { e.preventDefault(); return false; }, false);
    
    // Clear images from the state
    this.props.clearImages();

    this.props.layers.map(this.loadAssets);
    
    

  }




  loadAssets( url, index ){
    
    let _this = this;
    
    let image     = new Image();
        image.src = url;
    
    image.onload = function(){
    
      _this.props.updateOptions(image, index, _this );

      // All 3 images loaded - getOptions and start drawing
      if(_this.props.images.length == 3){
        
        _this.props.makeRequest('GET_OPTIONS', 'getOptions', {id:_this.props.id});
        _this.optionsLoading = true;
        
        requestAnimationFrame(_this.draw);
      }

    }

  }



  draw(){


    // Check for refs
    if( this.refs.canvas ){
      this.ctx.clearRect(0,0,this.refs.canvas.width,this.refs.canvas.height);
    }
    

    // CHECK IF WE HAVE ANY ASSETS
    if( this.props.images.length ){

      for (var i = this.props.images.length-1; i >=0; i--) {

        if( !this.props.images[i]){
          continue;
        }

        // DRAGGABLE THUMB
        if( i == 2){
            this.ctx.drawImage(
            this.props.images[i].image,
            this.props.images[i].x - (this.props.images[i].width * this.props.images[i].scale)/2, 
            this.props.images[i].y - (this.props.images[i].height * this.props.images[i].scale)/2, 
            this.props.images[i].width * this.props.images[i].scale, 
            this.props.images[i].height * this.props.images[i].scale 
          );
        // SHIRT AND BG    
        }else{
            this.ctx.drawImage(
            this.props.images[i].image,
            this.props.images[i].x, 
            this.props.images[i].y,
            this.props.images[i].width * this.props.images[i].scale , 
            this.props.images[i].height * this.props.images[i].scale
          );    
        }
            
      }
  
    }

    // DRAW DASHED LINE & WHITE BG LINE ON HOVER
    if( this.props.main.hover){

          this.ctx.restore();
          this.ctx.strokeStyle='#ffffff';
          this.ctx.beginPath();
          this.ctx.rect(
            this.props.stageSize.left + 0.5,  // add half pixel to fix anti aliasing - make it sharp
            this.props.stageSize.top + 0.5,   // add half pixel to fix anti aliasing - make it sharp
            this.props.stageSize.width,
            this.props.stageSize.height
          );
          this.ctx.closePath();
          this.ctx.stroke();

          this.ctx.save();
          this.ctx.setLineDash([5]);
          this.ctx.beginPath();
          this.ctx.strokeStyle='#000000';
          this.ctx.rect(
            this.props.stageSize.left + 0.5,  // add half pixel to fix anti aliasing - make it sharp
            this.props.stageSize.top + 0.5,   // add half pixel to fix anti aliasing - make it sharp
            this.props.stageSize.width,
            this.props.stageSize.height
          );
          this.ctx.closePath();
          this.ctx.stroke();

    }


    
    requestAnimationFrame(this.draw);
    
  }



  componentWillReceiveProps(nextProps){
    if( this.optionsLoading  ){

      if( !nextProps.options.isLoading ){

        if( !nextProps.options.error && nextProps.options.payload){
            
            let data = nextProps.options.payload.data;
            
            this.props.updateThumb([
              { property: 'scale', value: data.scale },
              { property: 'x', value: data.x },
              { property: 'y', value: data.y },
            ]);
            
        }

        this.optionsLoading = false;
      }

    }
   
  }




  saveArtwork( event ){

    let image = this.props.images[2],
        updates = { update: this.props.id, settings:{x:image.x, y:image.y, scale:image.scale}};
    
    this.props.makeRequest('UPDATE_OPTIONS', 'handleImages', updates);
    

  }



  updateScale( event ){

    this.props.updateThumb([
      { property: 'scale', value: event.target.value }
    ])

  }


  mouseEvent(event){
    this.props.handleMouseEvents(event, this.refs.canvas);
  }



  render(){


    return (
      <span>
       
        <canvas id='app'  
          width         = { this.props.canvasSize.width } 
          height        = { this.props.canvasSize.height } 
          className     = 'pull-left'
          ref           = 'canvas'
          onMouseDown   = { this.mouseEvent }
          onMouseUp     = { this.mouseEvent }
          onMouseLeave  = { this.mouseEvent }
          onMouseMove   = { this.mouseEvent }
          onMouseOver   = { this.mouseEvent }

        ></canvas>

        <div className='controls'>

          <input 
            id        ='range-slider' 
            onChange  = { this.updateScale } 
            type      = 'range'
            step      = '0.001' 
            min       = '1' 
            max       = '10'
            value     = { this.props.images[2] ? this.props.images[2].scale : 1 } 
          />

          <button 
            className   = 'btn btn-primary full-w' 
            type        = 'submit' 
            onClick     ={ this.saveArtwork }
          >
            Save artwork
          </button>

        </div>{/*.controls */}
              
      </span>
    );
  }

}


function mapStateToProps( {  images, main, options } ){
  return { images, main, options }
}

function mapDispatchToProps( dispatch ){
  return bindActionCreators({ makeRequest, updateOptions, handleMouseEvents, updateThumb, clearImages }, dispatch);
}


export default connect( mapStateToProps, mapDispatchToProps )( Canvas );
