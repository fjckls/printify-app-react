import React, { Component }       from 'react';
import { connect }                from 'react-redux';
import { bindActionCreators }     from 'redux';


/**
 * ACTION CREATORS
 */
import { makeRequest }              from '../actions/async/actionRequest';

/**
 * COMPONENTS
 */



class Uploader extends Component{

  constructor(){
    super();
    this.fileSelected = this.fileSelected.bind(this);
  }




  // Send file to the server
  fileSelected( event ){

    var formData = new FormData();                  
    formData.append('file', event.target.files[0]);
    
    this.props.makeRequest('GET_IMAGES', 'handleImages', formData);
  
  }



  render(){

    return (
      <span>

        <li className='img-thumbnail upload-btn'>
          
          <span className='glyphicon glyphicon-cloud-upload' aria-hidden='true'></span>
          <br/>CLICK TO UPLOAD IMAGE
          
          <input type='file' id='imageLoader' name='imageLoader' onChange={ this.fileSelected }/>

        </li>
      
      </span>
    );
  }

}


function mapStateToProps( {  uploadImage } ){
  return { uploadImage }
}

function mapDispatchToProps( dispatch ){
  return bindActionCreators({ makeRequest }, dispatch);
}


export default connect( mapStateToProps, mapDispatchToProps )( Uploader );
