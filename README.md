### Printify React app ###

* Install dependencies: **npm install**
* In ***public/index.html*** define base url for Api requests inside ***<body data-baseurl='...'>*** 
* Run: **npm start**
* Build: **npm run build-production**